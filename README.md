# Initial app setup

- Add to development block: `gem 'rubocop', '~> 0.55.0', require: false`
- bundle
- create .rubocop.yml at the root directory
- open .rubocop.yml and copy the contents of [this link](https://bitbucket.org/alan_maule/junglescout-rubocop/raw/master/.rubocop.yaml) to this file
- create .rubocop_app_overrides.yml at the root directory
- open .rubocop_app_overrides.yml and set your Specific Ruby version (EX):
```
AllCops:
  TargetRubyVersion: 2.5
``` 
- Open CMD Line and run: rubocop --auto-gen-config --exclude-limit=100000
	* The exclude-limit by default is set to 15. After 15 violations it starts turning off cops (We do not want this)
	* Generates .rubocop.yml and rubocop_todo.yml
	* rubocop_todo.yml silences all existing rubocop violations, EXCEPT for: Metrics/LineLength and Metrics/LineLength Violations 	
- Add this line to your gitignore: `.rubocop-https--*-yaml` (Ignores the cache file)

# Making app specific overrides

#### This should only be used to exclude specific files, not make your own app rules
App specific rules should live in .rubocop_app_overrides.yml

# Linter setup
Download your editor's rubocop package and follow the guide

# GOTCHAS
- Running rubocop will show all Metrics/LineLength and Metrics/LineLength violations for all files within your codebase. This is unavoidable with the way rubocop works
- The way to check all files ignoring the line length/method length issue is to run: rubocop --except Metrics/LineLength,Metrics/MethodLength

# Keeping your new code in line with our ruby standards

- All new code should pass a rubocops standard check
- You can check this by running rubocop directly against the file: `rubocop file/path.rb` or `rubocop --except Metrics/LineLength,Metrics/MethodLength --parallel`
- Your editor should also display any rubocop violations

# Links
[Original Ruby Style Guide](https://github.com/bbatsov/ruby-style-guide)
[Rubocop Docs](http://rubocop.readthedocs.io/en/latest/)
[Rubocop Config](https://github.com/bbatsov/rubocop/blob/master/manual/configuration.md)
